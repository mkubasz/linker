﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Linker
{
    public partial class LinkForm : Form
    {
        public List<Links> links = new List<Links>();
        private FileOperation foperat = new FileOperation();

        public LinkForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Application.Exit();
        }

        private void openFileToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            links = foperat.FileOpen();
            foreach (Links lin in links)
            {
                lBox.Items.Add(lin.Show());
            }
        }

        private void lBox_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void lBox_DoubleClick(object sender, EventArgs e)
        {
            if (lBox.SelectedItem != null)
                System.Diagnostics.Process.Start(lBox.SelectedItem.ToString());
        }

        private void lBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control == true)
            {
                if (e.KeyCode == Keys.V)
                {
                    try
                    {
                        links.Add(new Links(Clipboard.GetText()));
                        lBox.Items.Clear();
                        foreach (Links lin in links)
                        {
                            lBox.Items.Add(lin.Show());
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Brak skopiowanego adresu");
                    }
                }
                if (e.KeyCode == Keys.N)
                {
                    this.openFileToolStripMenuItem1_Click(sender, e);
                }
                if (e.KeyCode == Keys.Q)
                {
                    Application.Exit();
                }
            }
        }

        private void lBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (lToday.SelectedItem != null)
            {
                lToday.SelectedItem = null;
            }
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            foreach (var item in lBox.SelectedItems)
            {
                lToday.Items.Add(item);
            }
        }

        private void bSelected_Click(object sender, EventArgs e)
        {
            if (lToday.SelectedItem != null)
            {
                lToday.Items.RemoveAt(lToday.SelectedIndex);
                lToday.Refresh();
            }
            if (lBox.SelectedItem != null)
            {
                lBox.Items.RemoveAt(lBox.SelectedIndex);
                lBox.Refresh();
            }
        }

        private void lToday_MouseClick(object sender, MouseEventArgs e)
        {
            if (lBox.SelectedItem != null)
            {
                lBox.SelectedItem = null;
            }
        }

        private void bPath_Click(object sender, EventArgs e)
        {
            foperat.FileSave(links);
        }

        private void bSearch_Click(object sender, EventArgs e)
        {
        }
    }
}