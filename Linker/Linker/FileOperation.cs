﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Linker
{
    public class FileOperation
    {
        private System.IO.StreamReader fileRead;
        private System.IO.StreamWriter fileWrite;

        public string NameFile { get; set; }

        public FileOperation()
        {
        }

        public FileOperation(string tmp)
        {
            this.NameFile = tmp;
        }

        public List<Links> FileOpen()
        {
            List<Links> tmp = new List<Links>();
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            string line;
            openFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileRead = new System.IO.StreamReader(@openFileDialog1.FileName);
                while ((line = fileRead.ReadLine()) != null)
                    tmp.Add(new Links(line));
            }

            fileRead.Close();
            return tmp;
        }

        public void FileSave(List<Links> link)
        {
            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            fileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            fileDialog.FilterIndex = 2;
            fileDialog.RestoreDirectory = true;

            if (fileDialog.FileName != "")
            {
                fileWrite = new System.IO.StreamWriter(@fileDialog.FileName);
                foreach (Links item in link)
                {
                    fileWrite.WriteLine(item.Show());
                }
            }
        }
    }
}