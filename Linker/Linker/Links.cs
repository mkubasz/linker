﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Linker
{
	public class Links
	{
		private Uri link;

		public Links(string name)
		{
			Uri tmp = new Uri(createrLink(name));
			this.link = tmp;
		}

		public void Add(Uri tmp)
		{
			this.link = tmp;
		}
        /// <summary>
        /// Create url from string
        /// </summary>
        /// <param name="name"></param>
        /// <returns>string</returns>
		private string createrLink(string name)
		{
			string tmp;
			if (name.Substring(0, 4) == "http")
				tmp = name;
			else if (name.Substring(0, 3) == "www")
				tmp = @"http://" + name;
			else
			{
				tmp = @"http://www" + name;
			}
			return tmp;
		}

		public string Show()
		{
			return this.link.AbsoluteUri;
		}
		// TODO:
		public List<string> phreaseSearch(List<Links> link, string phrase)
	   {
		   List<string> listString = new List<string>();
		  // Match match
		   foreach (Links item in link)
			{
				//match = new Regex(phrase.ToLower(),);
				item.Show().ToLower();
			}
		   return listString;
	   }
	}
}