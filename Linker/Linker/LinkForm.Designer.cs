﻿namespace Linker
{
    partial class LinkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lBox = new System.Windows.Forms.ListBox();
            this.bSend = new System.Windows.Forms.Button();
            this.bAdd = new System.Windows.Forms.Button();
            this.lToday = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bCheck = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bSelected = new System.Windows.Forms.Button();
            this.bPath = new System.Windows.Forms.Button();
            this.bSearch = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lBox
            // 
            this.lBox.FormattingEnabled = true;
            this.lBox.ItemHeight = 16;
            this.lBox.Location = new System.Drawing.Point(32, 66);
            this.lBox.Name = "lBox";
            this.lBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lBox.Size = new System.Drawing.Size(658, 324);
            this.lBox.TabIndex = 0;
            this.lBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lBox_MouseClick);
            this.lBox.SelectedIndexChanged += new System.EventHandler(this.lBox_SelectedIndexChanged);
            this.lBox.DoubleClick += new System.EventHandler(this.lBox_DoubleClick);
            this.lBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lBox_KeyDown);
            // 
            // bSend
            // 
            this.bSend.Location = new System.Drawing.Point(740, 480);
            this.bSend.Name = "bSend";
            this.bSend.Size = new System.Drawing.Size(106, 69);
            this.bSend.TabIndex = 1;
            this.bSend.Text = "Send";
            this.bSend.UseVisualStyleBackColor = true;
            // 
            // bAdd
            // 
            this.bAdd.Location = new System.Drawing.Point(145, 393);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(147, 31);
            this.bAdd.TabIndex = 2;
            this.bAdd.Text = "Add";
            this.bAdd.UseVisualStyleBackColor = true;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // lToday
            // 
            this.lToday.AllowDrop = true;
            this.lToday.FormattingEnabled = true;
            this.lToday.ItemHeight = 16;
            this.lToday.Location = new System.Drawing.Point(32, 429);
            this.lToday.Name = "lToday";
            this.lToday.Size = new System.Drawing.Size(649, 132);
            this.lToday.TabIndex = 3;
            this.lToday.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lToday_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(303, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "All links";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 396);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Today lesson";
            // 
            // bCheck
            // 
            this.bCheck.Location = new System.Drawing.Point(703, 180);
            this.bCheck.Name = "bCheck";
            this.bCheck.Size = new System.Drawing.Size(143, 74);
            this.bCheck.TabIndex = 6;
            this.bCheck.Text = "Check Links";
            this.bCheck.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(895, 28);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openFileToolStripMenuItem1,
            this.saveFileToolStripMenuItem1,
            this.exitToolStripMenuItem1});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openFileToolStripMenuItem1
            // 
            this.openFileToolStripMenuItem1.Name = "openFileToolStripMenuItem1";
            this.openFileToolStripMenuItem1.Size = new System.Drawing.Size(191, 24);
            this.openFileToolStripMenuItem1.Text = "Open file Ctr + N";
            this.openFileToolStripMenuItem1.Click += new System.EventHandler(this.openFileToolStripMenuItem1_Click);
            // 
            // saveFileToolStripMenuItem1
            // 
            this.saveFileToolStripMenuItem1.Name = "saveFileToolStripMenuItem1";
            this.saveFileToolStripMenuItem1.Size = new System.Drawing.Size(191, 24);
            this.saveFileToolStripMenuItem1.Text = "Save file Ctr + S";
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(191, 24);
            this.exitToolStripMenuItem1.Text = "Exit Ctr + Q";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // bSelected
            // 
            this.bSelected.Location = new System.Drawing.Point(352, 391);
            this.bSelected.Name = "bSelected";
            this.bSelected.Size = new System.Drawing.Size(137, 33);
            this.bSelected.TabIndex = 9;
            this.bSelected.Text = "ClearSelected";
            this.bSelected.UseVisualStyleBackColor = true;
            this.bSelected.Click += new System.EventHandler(this.bSelected_Click);
            // 
            // bPath
            // 
            this.bPath.Location = new System.Drawing.Point(758, 451);
            this.bPath.Name = "bPath";
            this.bPath.Size = new System.Drawing.Size(75, 23);
            this.bPath.TabIndex = 10;
            this.bPath.Text = "Set Path";
            this.bPath.UseVisualStyleBackColor = true;
            this.bPath.Click += new System.EventHandler(this.bPath_Click);
            // 
            // bSearch
            // 
            this.bSearch.Location = new System.Drawing.Point(696, 66);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(106, 35);
            this.bSearch.TabIndex = 11;
            this.bSearch.Text = "Search";
            this.bSearch.UseVisualStyleBackColor = true;
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(696, 107);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(175, 22);
            this.txtSearch.TabIndex = 12;
            // 
            // LinkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 573);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.bPath);
            this.Controls.Add(this.bSelected);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.bCheck);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lToday);
            this.Controls.Add(this.bAdd);
            this.Controls.Add(this.bSend);
            this.Controls.Add(this.lBox);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "LinkForm";
            this.Text = "LinkerTools";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lBox;
        private System.Windows.Forms.Button bSend;
        private System.Windows.Forms.Button bAdd;
        private System.Windows.Forms.ListBox lToday;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bCheck;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFileToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem saveFileToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.Button bSelected;
        private System.Windows.Forms.Button bPath;
        private System.Windows.Forms.Button bSearch;
        private System.Windows.Forms.TextBox txtSearch;
    }
}

